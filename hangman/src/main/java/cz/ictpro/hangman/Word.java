package cz.ictpro.hangman;

import java.util.Arrays;

public class Word {

    private String word;
    private boolean[] uncover;

    public Word(String word) {
        this.word = word;
        this.uncover = new boolean[word.length()];
    }

    @Override
    public String toString() {
        var result = "";
        for(int i = 0; i < word.length(); i++) {
            if (uncover[i]) {
                result = result + word.charAt(i) + ' ';
            } else {
                result = result + "_ ";
            }
        }
        return result;
    }

    /**
     * Otestuje za je již celé sloho odhalené
     * @return <code>false</code> pokud alespoň jedno pismeno je stále skryté.
     */
    public boolean isUncover() {
        for (boolean b: uncover) {
            if (b == false) return false;
        }
        return true;
    }

    /**
     * Pokud je písmeno ve slově, je odkyré.
     * @param letter adané písmono
     * @return true, pokud je odkryto alespoń jedno písmeno.
     */
    public boolean testLetter(String letter) {
        var ch = letter.toUpperCase().charAt(0);
        var result = false;
        for (int i = 0; i < word.length(); i++) {
            if (!uncover[i] && word.charAt(i) == ch) {
                result = true;
                uncover[i] = true;
            }
        }
        return result;
    }

    public String getWord() {
        return word;
    }
}
