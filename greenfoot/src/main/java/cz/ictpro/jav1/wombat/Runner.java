package cz.ictpro.jav1.wombat;

import bh.greenfoot.runner.GreenfootRunner;
import cz.ictpro.jav1.wombat.greenfoot.WombatWorld;

public class Runner extends GreenfootRunner {
    static {
        bootstrap(Runner.class,
               Configuration.forWorld(WombatWorld.class)
                    .projectName("Wombat")
                    .hideControls(true)
        );
    }

    public static void main(String[] args) {
        GreenfootRunner.main(args);
    }
}
