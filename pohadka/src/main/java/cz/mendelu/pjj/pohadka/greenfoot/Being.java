package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;
import greenfoot.World;

import java.util.Optional;

public abstract class Being extends Actor {

    private int zivoty;

    public Being(int zivoty) {
        this.zivoty = zivoty;
    }

    public boolean jeZivy() {
        return zivoty > 0;
    }

    public int zran(int kolik) {
        this.zivoty = (zivoty > kolik) ? zivoty - kolik : 0;
        if (zivoty == 0) {
            var corps = toCorpse();
            if (corps.isPresent()) {
                World world = getWorld();
                world.addObject(corps.get(), getX(), getY());
                world.removeObject(this);
            } else {
                setImage("images/dead.png");
            }
        }
        return zivoty;
    }

    protected abstract Optional<Corpse> toCorpse();
}
