package cz.ictpro.jav1.wombat.greenfoot;

import greenfoot.Actor;

public class Leaf extends Actor {

    /**
     * Konstruktor pro vytvoření listu
     */
    Leaf() {
        setImage("images/leaf.gif");
    }
}
