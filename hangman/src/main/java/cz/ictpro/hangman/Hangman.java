package cz.ictpro.hangman;


public class Hangman {

    public static final int START_LIVES = 10;

    public static void main(String[] args) {
        var hangman = new Hangman();
        hangman.run();
    }

    private int lives;
    private Word word;
    private Console console;

    Hangman() {
        this.lives = START_LIVES;

        var dict = new Dictionary("cs_CZ.dic");
        this.word = new Word(dict.getRandomWord("H"));

        this.console = new Console();
    }

    public void run() {
        while (lives > 0) {
            console.printf("Počet životů: %d/%d\n", lives, START_LIVES);
            console.printf("Hádané slovo: %s\n", word);
            var letter = console.readLine("Zadejn písmeno: ");
            if (word.testLetter(letter)) {
                if (word.isUncover()) {
                    console.printf("*----------*\n");
                    console.printf("| YOU  WIN |\n");
                    console.printf("*----------*\n");
                    console.printf("Slovo: %s\n", word.getWord());
                    return;
                }
            } else {
                lives--;
            }
        }
        console.printf("*-----------*\n");
        console.printf("| YOU  LOSE |\n");
        console.printf("*-----------*\n");
        console.printf("Slovo: %s\n", word.getWord());
    }
}
