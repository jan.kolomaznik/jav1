package cz.mendelu.pjj.pohadka.greenfoot;

public class MovementException extends Exception {

    public MovementException(String message) {
        super(message);
    }

    public MovementException(String message, Throwable cause) {
        super(message, cause);
    }
}
