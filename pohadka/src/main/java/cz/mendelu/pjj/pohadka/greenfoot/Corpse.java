package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;

public class Corpse extends Actor {

    private int lektvar;
    private int artefakt;

    public Corpse(int lektvar, int artefakt) {
        this.lektvar = lektvar;
        this.artefakt = artefakt;
        setImage("images/dead.png");
    }
}
