package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;

import java.util.Optional;
import java.util.Random;

public class Monster extends Being implements Enemy {

    private Random kostka = new Random();

    private int sila;

    Monster(int zivoty, int sila) {
        super(zivoty);
        this.setImage("images/dragon.png");
        this.sila = sila;
    }

    @Override
    public void branSe(int utok) {
        zran(utok);
    }

    @Override
    public int odveta() {
        return sila + kostka.nextInt(6);
    }

    @Override
    protected Optional<Corpse> toCorpse() {
        return Optional.of(new Corpse(sila, 0));
    }
}

