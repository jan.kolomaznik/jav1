package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;
import greenfoot.World;

import java.util.Random;

public class BattleField extends World {
    /**
     * Create a new world with 8x8 cells and
     * with a cell size of 80x80 pixels
     */
    public BattleField() {
        super(8, 8, 80);
        //setBackground("grass.png");

        var pepa = new Rytir("Pepa", 10, 1);
        addObject(pepa, 0, 0);

        addObject(new Archer(5, 2), 2, 4);
        addObject(new Archer(6, 1), 2, 7);
        addObject(new Monster(7, 1), 6, 7);
        addObject(new Monster(6, 2), 5, 1);
        //addObject(new Being(100), 4, 4);

        Random random = new Random();
        int treeCount = 0;
        while (treeCount < 6) {
            int x = random.nextInt(8);
            int y = random.nextInt(8);
            boolean empty = getObjectsAt(x, y, Actor.class).isEmpty();
            if (empty) {
                addObject(new Tree(), x, y);
                treeCount++;
            }
        }
    }
}