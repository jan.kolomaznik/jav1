package cz.ictpro.jav1.wombat.greenfoot;

import greenfoot.Actor;

public class Rock extends Actor {

    Rock() {
        setImage("images/rock.gif");
    }
}
