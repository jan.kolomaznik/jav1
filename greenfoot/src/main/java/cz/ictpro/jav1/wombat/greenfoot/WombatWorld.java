package cz.ictpro.jav1.wombat.greenfoot;

import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.World;

import java.util.Random;

public class WombatWorld extends World {

    /**
     * Konstruktor
     */
    public WombatWorld() {
        super(8, 8, 60);
        setBackground("images/cell.jpg");
    }
}
