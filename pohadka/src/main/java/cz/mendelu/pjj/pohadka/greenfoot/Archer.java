package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;

import java.awt.*;
import java.util.Optional;

public class Archer extends Being implements Enemy {

    private int obratnost;

    Archer(int zivoty, int obratnost) {
        super(zivoty);
        this.obratnost = obratnost;
        this.setImage("images/archer.png");
    }

    @Override
    public void branSe(int utok) {
        if (!jeZivy()) {
            throw new UnsupportedOperationException("Don't attack to dead people.");
        }
        if (utok > obratnost) {
            zran(utok);
        }
    }

    @Override
    public int odveta() {
        return 0;
    }

    @Override
    protected Optional<Corpse> toCorpse() {
        return Optional.empty(); //new Corpse(0, obratnost);
    }
}
