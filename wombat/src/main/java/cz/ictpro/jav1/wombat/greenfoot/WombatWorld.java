package cz.ictpro.jav1.wombat.greenfoot;

import greenfoot.Actor;
import greenfoot.Greenfoot;
import greenfoot.World;

import java.util.Random;

public class WombatWorld extends World {

    private Wombat wombat;

    /**
     * Konstruktor
     */
    public WombatWorld() {
        super(8, 8, 60);
        setBackground("images/cell.jpg");

        // Vytvoření a umístění Wombata do světa.
        wombat = new Wombat();
        addObject(wombat, 3, 3);
        //wombat.step();
        //wombat.step();

        for (int i = 0; i < 5; i++) {
            addObjectToRandomPosition(new Leaf());
        }

        for (int i = 0; i < 10; i++) {
            addObjectToRandomPosition(new Rock());
        }
    }

    private Random rand = new Random(1); // <-- Vytvořní generátoru nahodných čísel

    private void addObjectToRandomPosition(Actor actor) {
        int x = rand.nextInt(8);
        int y = rand.nextInt(8);
        while (!getObjectsAt(x, y, Actor.class).isEmpty()) {
            x = rand.nextInt(8);
            y = rand.nextInt(8);
        }

        addObject(actor, x, y);
    }

    public void act() {
        String key = Greenfoot.getKey();
        if (key == "right") {
            wombat.step(1, 0);
        }
        if (key == "left") {
            wombat.step(-1, 0);
        }
        if (key == "up") {
            wombat.step(0, -1);
        }
        if (key == "down") {
            wombat.step(0, 1);
        }
    }
}
