package cz.mendelu.pjj.pohadka.greenfoot;

public interface Enemy {
    
    void branSe(int utok);

    boolean jeZivy();

    int odveta();
}
