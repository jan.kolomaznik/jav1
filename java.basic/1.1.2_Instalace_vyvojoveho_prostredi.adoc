= Instalace vývojového prostředí
:revealjs_theme: white

== Co budeme potřebovat
* Balík vývojových programů Java Development Kit (JDK)
* Hodný editor nebo rovnou IDE
* Několik podpůdných nástrojů
  - GIT
  - Gradle/Maven

== Instalace JDK
* Vše, co je třeba udělat po stažení JDK z Internetu, spočívá pouze v rozbalení souborů do zvoleného adresáře a nastavení cesty ke spustitelným souborům.
  - Také je možné použit jeden z dostupných instalčních nástrojů.
* Distribuce pro jednotlivé operační systémy se příliš neliší.
  Verze pro Windows 95/NT je bezproblémová, u starších verzí OS Linux je případně nutno provést upgrade některých systémových knihoven.

=== Volba JDK
* K dispozici několik implementaci JDK
  - Od firmy Oracle: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
  - Od OpenJDK: http://openjdk.java.net/install/
* Kromě nejnovější verze JDK se doporučuje stáhnout si i originální dokumentaci (samozřejmě v angličtině), zejména referenční popis Java Core API.

== Instalace vývojového prostředí
* V javě je možné vytvářet programy pomocí textového editoru a příkazové řádky.
* Je možné ale použít některý z řady IDE, mnohé jsou zdarma

=== Eclipse
* Eclipse, který byl asi od roku 2001, byl nesmírně populární s vývojáři v Javě.
* Obsahuje řadu užitečných pluginů.
* Nejlepším aspektem této platformy je jeho schopnost konfigurace růůznách pohledů.
* Eclipse nabízí široký výběr variant ličících se hlavně předinstalovanými pluginy.
* **Ke stažení: http://www.eclipse.org**

=== IntelliJ IDEA
* Ještě další populární a zdarma IDE pro vývojáře v jazyce Java je JetBrains' IntelliJ IDEA.
* Nabízí podporu pro několik budování systémů, tato platforma také vyniká:
  - intuitivním doplňováním kódu, integrací v rámců a testovacích přístrojů, plně funkčním editorem databáze, UML Designer.
* Navíc tato platforma nabízí nástroje pro Android a iOS vývoj aplikací.
* **Ke stažení: https://www.jetbrains.com/idea/**

=== NetBeans
* NetBeans IDE nabízí různé pokročilé funkce a podporu pro Javu, PHP, C / C ++ a HTML5.
* Je podporobána firmou Oracle (vlastníkem jazyka Java)
* Pomáhá vývojářům rychle a snadno vytvářet desktopové, webebové a mobilní aplikace.
* Tato platforma, která se může pochlubit celou celosvětovou komunitu vývojářů, je open source.
* **Ke stažení: https://netbeans.org/**

=== JDeveloper
* Vyvinutý společností Oracle JDeveloper je silný IDE, který zjednodušuje proces vývoje SOA a EE aplikací Java-based.
* To umožňuje vývoj v Javě, SQL, XML , HTML , JavaScript, PHP a další.
* Pokrývající celý životní cyklus vývoje od návrhu, vývoje kódu, ladění, optimalizace, profilování a nasazení, platforma se zaměřuje na zjednodušení vývoje aplikací v maximální možné míře.
* **Ke stažení: http://www.oracle.com/technetwork/developer-tools/jdev/overview/index.html**

=== BlueJ
* Vývojové prostředí navržené pro výuku jazyka Java.
* Vzhledem k tomu, IDE je nejlepší pro začínající vývojáře, je doporučeno, aby se připojily k Blueroom, což je komunita, která mohou uživatelům pomoci pochopit software a najít podporu.
* Můžete nainstalovat několik rozšířeními BlueJ, aby to jinak než u standardního programu, jako je vzdálený správce souborů a multi-projektového pracovního prostoru.
* **Ke stažení: https://www.bluej.org/**

== GIT
* Pro práci s kódem je vhodné použít nástroj pro jeho verzování a zálohování.
* Nejpoužívanějším nástrojem je v dnešní době https://git-scm.com/[GIT]
* Zle použít pomocí bash nebo nějakou nádstavbu:
  - https://www.sourcetreeapp.com/[SourceTree]
  - https://www.gitkraken.com[GitKraken]

== Gradle/Maven
* Dalším nástrojem, který se hodí, ale není nezbytný je nástroj na správu projektu.
* Tyto nástoje zjednodušují vývoj, zprávu závislostí a nasazení:
  - https://maven.apache.org/[Maven]
  - https://gradle.org/[Gradle]