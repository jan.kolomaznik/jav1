package cz.ictpro.hangman;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Random;

/**
 * Slovnik obsahující všechna slova daného jazyka
 */
public class Dictionary {

    private static final int NUMBER_OF_TRY = 1000;

    public static void main(String[] args) {
        var dict = new Dictionary("cs_CZ.dic");
        //var words = dict.readFileToMemory();
        //System.out.println(Arrays.toString(words));
        //System.out.printf("%s: %s\n", "žlučník/H", dict.checkWorld("žlučník/H", "H"));
        //System.out.printf("%s: %s\n", "pokus/HN", dict.checkWorld("pokus/HN", "H"));
        //System.out.printf("%s: %s\n", "žloutnutý/YKRN", dict.checkWorld("žloutnutý/YKRN", "H"));
        System.out.println(dict.getRandomWord("H"));
        System.out.println(dict.getRandomWord("H"));
        System.out.println(dict.getRandomWord("H"));
    }

    private String fileName;
    private Random random = new Random();

    public Dictionary(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Vybere ze slovníku jedno slovo, které odpovídá zadaným specifikacím.
     * @param flag Seznam velkých písmen vyskytujících se za slovem po /, například "H"
     * @return jedno náhodné slovo
     * @throws IllegalStateException pokud není možné najít vyhovující slovo.
     */
    public String getRandomWord(String flag) {
        var words = readFileToMemory();
        for (int i = 0; i < NUMBER_OF_TRY; i++) {
            var world = words[random.nextInt(words.length)];
            if (checkWorld(world, flag)) {
                return unification(world);
            }
        }
        throw new IllegalStateException("Word wasn't found with flag");
    }

    private String unification(String word) {
        word = word.toUpperCase();
        //word = word.split("/", 2)[0];
        word = word.substring(0, word.indexOf('/'));
        word = StringUtils.stripAccents(word);
        return word;
    }


    private String[] readFileToMemory() {
        //var is = ClassLoader.getSystemResourceAsStream(fileName);
        try {
            var fis = new FileInputStream(fileName);
            //for (int i = 0; i < 100; i++) {
            //    System.out.println((char)fis.read());
            //}
            var isr = new InputStreamReader(fis, "ISO-8859-2");
            var br = new BufferedReader(isr);
            //for ( int i = 0; i < 100; i++) {
            //    System.out.println(br.readLine());
            //}
            var lines = Integer.parseInt(br.readLine());
            var result = new String[lines];
            for (int i = 0; i < lines; i++) {
                result[i] = br.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalStateException("Can't get list of word.", e);
        }
    }

    private boolean checkWorld(String word, String flag) {
        return word.endsWith("/" + flag);
        //var split = word.split("/");
        //return split.length == 2 ? split[1].contains(flag) : false;
    }

}
