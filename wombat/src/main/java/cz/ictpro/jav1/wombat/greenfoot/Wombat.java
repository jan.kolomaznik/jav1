package cz.ictpro.jav1.wombat.greenfoot;

import greenfoot.Actor;
import greenfoot.GreenfootSound;

public class Wombat extends Actor {

    /**
     * Konstruktor, tedy kod který se volá, když se objekt vytváří.
     */
    Wombat() {
        setImage("images/wombat.gif");
    }

    /**
     * Krok dopředu je metoda (na objektu)
     */
    void step(int dx, int dy) {
        relativeMovementInDirection(dx, dy);
        if (isTouching(Rock.class)) {
            relativeMovementInDirection(-dx, -dy);
        }
        updateImageByDirection(dx);
        if (isTouching(Leaf.class)) {
            eatTouchingLeaf();
        }
    }

    /**
     * Pohne wombatem v požadovaném směru podle relativních souřadnic dx, dy.
     * @param dx (1 = left, -1 right, 0 no move)
     * @param dy (1 = down, -1 up, 0 no move)
     */
    private void relativeMovementInDirection(int dx, int dy) {
        var x = getX() + dx;
        var y = getY() + dy;
        setLocation(x, y);
    }

    /**
     * Podle horizontálního smeru (left <-> right) aktualizuje obrázek wombata.
     * @param direction (záporné = right, kladne = left, 0 = no change)
     */
    private void updateImageByDirection(int direction) {
        if (direction > 0) {
            setImage("images/wombat.gif");
        } else if (direction < 0) {
            setImage("images/wombat-left.gif");
        }
    }

    /**
     * Sní jeden lístek, kterého se právě dotýká.
     */
    private void eatTouchingLeaf() {
        var leaf = getOneIntersectingObject(Leaf.class);
        getWorld().removeObject(leaf);
    }

}
