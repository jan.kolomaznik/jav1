package cz.mendelu.pjj.pohadka.greenfoot;

import greenfoot.Actor;
import greenfoot.Greenfoot;

import java.util.Optional;
import java.util.Random;

public class Rytir extends Being {

    private Random kostka = new Random();

    private String name;
    private int zbran;

    Rytir(String name, int zivoty, int zbran) {
        super(zivoty);
        this.setImage("images/knight.png");
        this.name = name;
        this.zbran = zbran;
        System.out.printf("Zrození rytíře %s, \n\tživoty: %d, \n\tzbraň: %d.\n", name, zivoty, zbran);
    }

    /**
     * Provede utok na vybraného nepřítele
     * @param enemy
     */
    void zautoc(Enemy enemy) {
        var utok = zbran + kostka.nextInt(6);
        enemy.branSe(utok);
        System.out.printf("Rytiř %s utočí na %s\n\tsila utoku: %d\n", name, enemy, utok);
        if (enemy.jeZivy()) {
            var odveta = enemy.odveta();
            var zraneni = (zbran < odveta) ? odveta - zbran : 0;
            var zivoty = zran(zraneni);
            System.out.printf("\todveta: %d zivotů, zbývá mi %d.\n", zraneni, zivoty);
        } else {
            System.out.printf("\tnepřítel je mrtví.\n");
        }
    }

    void movement(int dx, int dy) throws MovementException {
        try {
            // Když na pozici nikdo není, tak se tam pohnu
            if (getObjectsAtOffset(dx, dy, Enemy.class).isEmpty()) {
                setLocation(getX() + dx, getY() + dy);
            } else {
                // Když je pozice obsazená nepřítem, utočím
                Enemy enemy = (Enemy) getOneObjectAtOffset(dx, dy, Enemy.class);
                zautoc(enemy);
            }
        } catch (Exception e) {
            throw new MovementException("Can't move.", e);
        }
    }

    @Override
    public void act() {
        try {
            String key = Greenfoot.getKey();
            if (key == "left") movement(-1, 0);
            else if (key == "right") movement(1, 0);
            else if (key == "up") movement(0, -1);
            else if (key == "down") movement(0, 1);
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (MovementException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Optional<Corpse> toCorpse() {
        return Optional.empty();
    }
}
