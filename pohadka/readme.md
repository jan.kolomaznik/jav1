[remark]:<class>(center, middle)
# Programovací jazyk Java
## Cvičení 2

[remark]:<slide>(new)
## Co dnes procvičíme

Vytvoření jednoduchého datového objektu tzv: POJO

* Konstruktory
* Klíčové slovo this

Procvičení implementace rozhraní a abstraktní třídy.

* Dědičnost a konstruktory
* Práce s metodami (překrytí, přetížení)

Statické metody

* Vytvoření jedináčka a identifikátory.

[remark]:<slide>(wait)
* Ukážeme se framework GreenFoot


[remark]:<slide>(new)
## Pohádka o drakovy

V příběhu existuje jediný drak a jmenuje se Šmak, nikdo nemůže odolat jeho síle deseti mužů a jeho šupiny jsou jak 
neprominutelné štíty.

Zástupy statečných hrdinů se pokoušeli získat jeho poklad. Byli mezi nimi stateční rytíři s ostrými meči i obratní 
hraničáři s dlouhými luky. Rivalita mezi nimi byla taková, že nedokázali spojit své síly a často bojovali o to, kdo se 
s drakem utká první.

Až jednou se na …

[remark]:<slide>(new)
### Postup práce
1. Stáhněte si kostru projektu [pohadka.zip](./pohadka.zip)
   * Nezapoňte nastavit java JDK!
   * Aplikaci si spusťe.
2. Navrhneme diagram tříd
   * Použijte kostru tříd z `src/main/java/cz.mendelu.pjj.pohadka.domain`
   * Doplňte jej o metody a vazby.
3. Seznámíme se s Greenfoot frameworkem
   * Kam se ztratila main metoda?
   * Rozdělení tríd na `GUI` - `domain`
3. Implementace v Jidei
   * Dopňte navržené metody
